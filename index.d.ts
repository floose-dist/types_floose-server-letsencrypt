/// <reference types="node" />
import {Framework, Utils} from "floose";
import Schema = Utils.Validation.Schema;
import Server = Framework.Server;
import {SecureContext} from "tls";

export declare interface LetsencryptServerConfig {
    environment: "staging" | "production";
    email: string;
    domains: string[];
}

export declare class LetsencryptServer extends Server {
    readonly configurationValidationSchema: Schema;
    readonly running: boolean;
    init(config: LetsencryptServerConfig): Promise<void>;
    start(): Promise<void>;
    stop(): Promise<void>;
    getSNICallback(): ReturnType<() => (hostname: string, callback: (err: Error, context?: SecureContext) => void) => void>
}